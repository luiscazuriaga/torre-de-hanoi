let discNumber = 3;
let moveCounter = 0;
let timer = 0;


const main = (discNumber) => {
    generateDivs(discNumber);
}

const generateDivs = (discNumber) => {
    // Cria o container principal
    let mainContainer = document.createElement("div");
    mainContainer.id = "mainContainer";
    document.body.appendChild(mainContainer);

    // Cria container das torres
    let towersContainer = document.createElement("div");
    towersContainer.id = "towersContainer";
    mainContainer.appendChild(towersContainer);

    // Cria container dos discos
    let discContainer = document.createElement("div");
    discContainer.id = "discContainer";
    mainContainer.appendChild(discContainer);

    //Container dos domos, para posicionamento sobreposto ao resto
    let DomeContainer = document.createElement("div")
    DomeContainer.id = "DomeContainer"
    discContainer.appendChild(DomeContainer)

    //Cria domos
    let discBoxDome0 = document.createElement("div")
    discBoxDome0.className = "discBoxDome"
    discBoxDome0.id = "discBoxDome0"
    DomeContainer.appendChild(discBoxDome0)

    let discBoxDome1 = document.createElement("div")
    discBoxDome1.className = "discBoxDome"
    discBoxDome1.id = "discBoxDome1"
    DomeContainer.appendChild(discBoxDome1)

    let discBoxDome2 = document.createElement("div")
    discBoxDome2.className = "discBoxDome"
    discBoxDome2.id = "discBoxDome2"
    DomeContainer.appendChild(discBoxDome2)

    // Gera container das imagens dos x
    let imageContainer = document.createElement("div");
    imageContainer.id = "imageContainer";
    mainContainer.appendChild(imageContainer);

    //Gera container de contador de movimentos
    let moveCounterDiv = document.createElement("div");
    moveCounterDiv.id = "moveCounterDiv";
    moveCounterDiv.innerHTML = `${moveCounter}`;
    mainContainer.appendChild(moveCounterDiv);

    //Gera container de timer
    let timerDiv = document.createElement("div");
    timerDiv.id = "timerDiv";
    timerDiv.innerHTML = `00:00`;
    mainContainer.appendChild(timerDiv);

    // gera imagens dos x
    let image = [];
    for (let i = 0; i < 3; i++) {
        image[i] = document.createElement("img");
        image[i].src = "./img/x.png"
        image[i].id = `img${i}`
        imageContainer.appendChild(image[i]);
    }
    //gera torres
    let discBox = [];
    let towers = [];
    for (let i = 0; i < 3; i++) {
        discBox[i] = document.createElement("div");
        discBox[i].id = `discBox${i}`;
        discBox[i].className = "discBox";
        discContainer.appendChild(discBox[i]);
        towers[i] = document.createElement("div");
        towers[i].id = `towers${i}`;
        towers[i].className = "towers";
        towersContainer.appendChild(towers[i]);
    }

    //gera discos
    let disc = [];
    for (let j = 1; j <= discNumber; j++) {
        disc[j] = document.createElement("div");
        disc[j].id = `disc${j}`;
        disc[j].className = "discs"
        discBox[0].appendChild(disc[j]);
    }
}

main(discNumber);

// handler de click para start screen
let startGameButton = document.getElementById('startGameButton')
let startScreen = document.getElementById('startScreen')

startGameButton.addEventListener('click', () => {
    startScreen.style.display = "none"
})

// handler de click para level option screen
let levelOptionScreen = document.getElementById("levelOptionScreen");
levelOptionScreen.addEventListener("click", (evt) => {
    let optionButton = evt.target.id;

    if (optionButton === "level1") {
        levelOptionScreen.style.display = "none";
        makeThediscImg(3)
        discNumber = 3
    }

    if (optionButton === "level2") {
        levelOptionScreen.style.display = "none";
        addDiscs(1);
        makeThediscImg(4)
        discNumber = 4
    }

    if (optionButton === "level3") {
        levelOptionScreen.style.display = "none";
        addDiscs(2);
        makeThediscImg(5)
        discNumber = 5
    }

    if (optionButton === "level4") {
        levelOptionScreen.style.display = "none";
        addDiscs(3);
        makeThediscImg(6)
        discNumber = 6
    }
});

// Função que adiciona discos de acordo com nível
const addDiscs = (numberOfDiscs) => {
    let discBox0 = document.getElementById("discBox0");
    let disc = [];
    for (let j = 1; j <= numberOfDiscs; j++) {
        disc[j] = document.createElement("div");
        disc[j].id = `disc${j + 3}`;
        disc[j].className = "discs"
        discBox0.appendChild(disc[j]);
    }
}

// Pega os elementos que contem os discos
let discBox0 = document.getElementById("discBox0");
let discBox1 = document.getElementById("discBox1");
let discBox2 = document.getElementById("discBox2");

// Variavel para saber se um caixa foi clicada e está selecionada
let selected0 = false;
let selected1 = false;
let selected2 = false;

// Evento de clique que indica para onde discos devem se mover
discBox0.addEventListener('click', (e) => {
    if (!selected0 && !selected1 && !selected2) {
        selected0 = true;
    } else if (selected0 && !selected1 && !selected2) {
        selected0 = false;
    }
    if (selected1 && !selected2) {
        moveBox("discBox1", "discBox0");
        selected0 = false;
        selected1 = false;
    }
    if (!selected1 && selected2) {
        moveBox("discBox2", "discBox0");
        selected0 = false;
        selected2 = false;
    }

    changeBoxColor()
});

discBox1.addEventListener('click', (e) => {
    if (!selected0 && !selected1 && !selected2) {
        selected1 = true;
    } else if (!selected0 && selected1 && !selected2) {
        selected1 = false;
    }

    if (selected0 && !selected2) {
        moveBox("discBox0", "discBox1");
        selected0 = false;
        selected1 = false;
    }
    if (!selected0 && selected2) {
        moveBox("discBox2", "discBox1");
        selected1 = false;
        selected2 = false;
    }

    if (selected2 === true) {
        discBox2.style.backgroundColor = "black"
    }

    changeBoxColor()
});

discBox2.addEventListener('click', (e) => {
    if (!selected0 && !selected1 && !selected2) {
        selected2 = true;
    } else if (!selected0 && !selected1 && selected2) {
        selected2 = false;
    }

    if (selected0 && !selected1) {
        moveBox("discBox0", "discBox2");
        selected0 = false;
        selected2 = false;
    }
    if (!selected0 && selected1) {
        moveBox("discBox1", "discBox2");
        selected1 = false;
        selected2 = false;
    }

    changeBoxColor();
    checkWinner(discNumber);
});

function changeBoxColor() {
    if (selected0 === true) {
        discBoxDome0.style.backgroundImage = "url('./img/cone04.png')"
    } else {
        discBoxDome0.style.backgroundImage = "url('./img/cone03.png')"
    }

    if (selected1 === true) {
        discBoxDome1.style.backgroundImage = "url('./img/cone04.png')"
    } else {
        discBoxDome1.style.backgroundImage = "url('./img/cone03.png')"
    }

    if (selected2 === true) {
        discBoxDome2.style.backgroundImage = "url('./img/cone04.png')"
    } else {
        discBoxDome2.style.backgroundImage = "url('./img/cone03.png')"
    }
}

// Função que move as caixas de container
const moveBox = (firstClicked, secondClicked) => {
    let disc = document.getElementById(firstClicked).lastChild;
    let discBox = document.getElementById(secondClicked);

    let checkin = checkIfPermited(disc, discBox);

    // inicia o timer
    if (moveCounter === 0) {
        timeInterval = setInterval(timerCounter, 1000);
    }

    // Verifica se jogada pode ser feita, se não, põe um x na tela
    if (checkin) {
        discBox.appendChild(disc)
        // contador de movimentos e coloca esse valor na tela
        moveCounter += 1;
        moveCounterDiv = document.getElementById("moveCounterDiv");
        moveCounterDiv.innerHTML = `${moveCounter}`;

    } else {
        errorImage = numberOfErrorImage(secondClicked);
        errorImageElement = document.getElementById(errorImage);
        setTimeout(() => {
            errorImageElement.style.visibility = "visible";
        }, 10);
        setTimeout(() => {
            errorImageElement.style.visibility = "hidden";
        }, 1000);
    }
    return timeInterval;
}

// Seleciona a box na qual a jogada errada foi feita
const numberOfErrorImage = (discBox) => {
    let discBoxValue = Number(discBox.match(/\d/g)[0])
    return `img${discBoxValue}`;
}

// função de contador timer
let minutos = 0;
const timerCounter = () => {
    timer += 1;
    if (timer === 60) {
        minutos += 1;
        timer = 0;
    }
    timerDiv = document.getElementById("timerDiv");
    if (minutos < 10 && timer < 10) {
        timerDiv.innerHTML = `0${minutos}:0${timer}`;
    } else if (minutos < 10 && timer > 10) {
        timerDiv.innerHTML = `0${minutos}:${timer}`;
    } else if (minutos > 10 && timer < 10) {
        timerDiv.innerHTML = `${minutos}:0${timer}`;
    } else {
        timerDiv.innerHTML = `${minutos}:${timer}`;
    }
    return timer;
}

// checa se um disco pode ser colocado em cima de outro
function checkIfPermited(disc, discBox) {
    let discValueFirst = Number(disc.id.match(/\d/g)[0])

    if (discBox.lastChild === null) {
        return true;
    } else {
        let discValueSecond = Number(discBox.lastChild.id.match(/\d/g)[0])
        return discValueFirst > discValueSecond;
    }
}
//div para todos so confetes
let confeteContainer = document.createElement('div')
confeteContainer.id = "confeteContainer"
mainContainer.appendChild(confeteContainer)

// checa se o jogador é vitorioso
const checkWinner = (discNumber) => {
    numberOfDiscs = document.querySelectorAll("#discBox2 > div");

    if (numberOfDiscs.length === discNumber) {
        console.log()
        //Colocar imagem do domo verde ao inves de alterar cor - luis
        discBoxDome0.style.backgroundImage = "url('./img/cone04.png')"
        discBoxDome1.style.backgroundImage = "url('./img/cone04.png')"
        discBoxDome2.style.backgroundImage = "url('./img/cone04.png')"

        for (let a = 0; a < 3; a++) {
            let imgVictoryDiv = document.createElement('div')
             imgVictoryDiv.className= "imgVictoryDiv"
            let imgVictory = document.createElement("img");
            imgVictory.src = "./img/confeti.gif";
            imgVictory.id = "imgVictory";
            imgVictoryDiv.appendChild(imgVictory);
            confeteContainer.appendChild(imgVictoryDiv)
        }        // Para contador
        clearInterval(timeInterval);
        return true;
    }
}

// STYLE
// imagem disco 1
const makeThediscImg = (numberOfImages) => {

    let j = numberOfImages
    for (let a = 1; a <= numberOfImages; a++) {

        let disc = document.getElementById(`disc${a}`);


        let bottomImg = document.createElement('div')
        bottomImg.id = `disc${a}bottomImg`
        bottomImg.className = "discsImgsBottom"
        bottomImg.style.backgroundImage = `url('./img/disc${j}-2.png')`

        disc.appendChild(bottomImg)

        let topImg = document.createElement('div')
        topImg.id = `disc${a}topImg`
        topImg.className = "discsImgsTop"
        topImg.style.backgroundImage = `url('./img/disc${j}-1.png')`
        disc.appendChild(topImg)
        j -= 1
    }
}

//info div 